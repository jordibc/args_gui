# args gui

This is a library to generate a
[GUI](https://en.wikipedia.org/wiki/Graphical_user_interface) from the
parser object of Python's
[argparse](https://docs.python.org/3/library/argparse.html).

The GUI that it generates can be either a
[GTK](https://python-gtk-3-tutorial.readthedocs.io/en/latest/) dialog
or a web page.

## Work in progress

This is basically a placeholder for something I would like to do.

Ideally, I'd like to pick any program that uses argparse and generate
automatically a web page with a nice gui that shows all the options.
Also, from that gui, I'd like to be able to select any option and get
the corresponding command line.

Some functionality is already there, but there's a lot missing.

## Test

You can run a simple test that displays a GTK dialog with options from
argparse and prints the selected ones with:

```sh
python args_gui.py
```
